// Update with your config settings.

/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */
module.exports = {

  development: {
    client: 'mysql2',
    connection: {
      database: 'my_db',
      host: '127.0.0.1',
      user: 'root',
      password: '',
      database: 'teste'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: 'database/migrations'
    }
  },

  staging: {
    client: 'mysql2',
    connection: {
      database: 'my_db',
      host: '127.0.0.1',
      user: 'root',
      password: '',
      database: 'teste'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: 'database/migrations'
    }
  },

  production: {
    client: 'mysql2',
    connection: {
      database: 'my_db',
      host: '127.0.0.1',
      user: 'root',
      password: '',
      database: 'teste'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: 'database/migrations'
    }
  }

};
