/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
 exports.up = (knex) => {
    return knex.schema.createTable('livros', (table) => {
        table.increments()
        table.string('nome', 60).notNullable()
        table.string('isbn', 20).notNullable()
        table.string('autor', 80).notNullable()
        table.string('editora', 60).notNullable()
        table.string('genero', 60).notNullable()
        table.string('genetroTroca', 60).notNullable()
    })
  
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = (knex) => knex.schema.dropTable('livros');