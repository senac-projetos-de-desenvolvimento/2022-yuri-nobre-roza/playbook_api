const express = require('express');
const routes = express.Router();
const cors = require('cors');
const LivroController = require('./controllers/LivroController');

routes.use(cors());

routes.get('/livros', LivroController.index)
        .post('/livros', LivroController.store)


module.exports = routes;