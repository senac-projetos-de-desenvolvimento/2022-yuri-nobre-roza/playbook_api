const knex = require('../database/dbConfig')

module.exports = {

    async store (req, res) {
        const { nome, isbn, autor, editora, genero, genetroTroca } = req.body;

        if (!nome || !isbn || !autor || !editora || !genero || !genetroTroca) {
            res.status(400).json({erro: 'Preencha todos os dados'});
            return;
        }

        try{
            const novo = await knex('livros').insert({nome, isbn, autor, editora, genero, genetroTroca})
            res.status(201).json({id: novo[0]})
        } catch (error){
            res.status(400).json({erro: error.message})
        }
    },

    async index (req,res){
        const livros = await knex('livros').select('l.id', 'l.nome', 'l.isbn', 'l.autor', 'l.editora', 'l.genero', 'l.genetroTroca')
                                            .from('livros as l').orderBy('l.id', 'asc')
        res.status(200).json(livros);
    }
}