const express = require('express')
const routes = require('./routes')
const app = express()
const port = 3001

app.get('/', (req, res) => {
    res.send('Hello world')
})

app.use(express.json())
app.use(routes)

app.listen(port, () => {
    console.log(`App listen http://localhost:${port}`)
})