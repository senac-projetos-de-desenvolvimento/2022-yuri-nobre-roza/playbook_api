Esse projeto foi desenvolvido usando JavaScript, NodeJS, Knex e MySQL.
Está sendo utilizada uma database chamada teste e as conexões com o banco estão rodando na porta 3001 - então libere a porta ou mude a configuração para utilizar outra porta.

Para rodar o projeto faça o download do repositório abra o terminal (via VSCode ou pelo cmd) e siga os passos abaixo:

1) npm i - para adicionar as dependências do projeto
2) npx knex migrate:up 20220519015352_create_table_livros.js - para rodar a migração que cria a tabela de livros
3) nodemon app - para rodar o projeto

Para testar você pode acessar o endereço contido no arquivo routes.js
